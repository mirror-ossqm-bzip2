# ------------------------------------------------------------------
# This file is part of bzip2/libbzip2, a program and library for
# lossless, block-sorting data compression.
#
# bzip2/libbzip2 version 1.0.5 of 10 December 2007
# Copyright (C) 1996-2007 Julian Seward <jseward@bzip.org>
#
# Please read the WARNING, DISCLAIMER and PATENTS sections in the 
# README file.
#
# This program is released under the terms of the license contained
# in the file LICENSE.
# ------------------------------------------------------------------

SHELL=/bin/sh

# To assist in cross-compiling
CC?=gcc
AR?=ar
RANLIB?=ranlib
STRIP?=strip
LDFLAGS=

BIGFILES=-D_FILE_OFFSET_BITS=64
CFLAGS=-Wall -Winline -O2 -g $(BIGFILES)
CFLAGS_PIC=-fpic -fPIC
DESTDIR?=$(INSTALL_ROOT)

# Where you want it installed when you do 'make install'
PREFIX?=/usr/local
BINDIR?=$(PREFIX)/bin
LIBDIR?=$(PREFIX)/lib
DATADIR?=$(PREFIX)/share
MANDIR?=$(DATADIR)/man
MAN1DIR?=$(MANDIR)/man1
INCLUDEDIR?=$(PREFIX)/include

OBJS= blocksort.o  \
      huffman.o    \
      crctable.o   \
      randtable.o  \
      compress.o   \
      decompress.o \
      bzlib.o

PIC_OBJS= blocksort.pic.o  \
      huffman.pic.o    \
      crctable.pic.o   \
      randtable.pic.o  \
      compress.pic.o   \
      decompress.pic.o \
      bzlib.pic.o

LIBVERSION_MAJOR=1
LIBVERSION_MINOR=0
LIBVERSION_PATCHLEVEL=4

SHLIB=libbz2.so
SONAME=$(SHLIB).$(LIBVERSION_MAJOR)
LIBNAME=$(LIBNAME2).$(LIBVERSION_PATCHLEVEL)
LIBNAME2=$(SONAME).$(LIBVERSION_MINOR)

all: libbz2.a bzip2 bzip2recover libbz2.so $(SONAME) $(LIBNAME2) $(LIBNAME)

$(LIBNAME): $(PIC_OBJS)
	$(CC) $(LDFLAGS) -shared -Wl,-soname -Wl,$(SONAME) -o $@ $(PIC_OBJS)

$(LIBNAME2): $(LIBNAME)
	ln -sf $(LIBNAME) $@

$(SONAME): $(LIBNAME2)
	ln -sf $(LIBNAME2) $@

$(SHLIB): $(SONAME)
	ln -sf $(SONAME) $@

bzip2: libbz2.a bzip2.o
	$(CC) $(CFLAGS) $(LDFLAGS) -o bzip2 bzip2.o -L. -lbz2

bzip2recover: bzip2recover.o
	$(CC) $(CFLAGS) $(LDFLAGS) -o bzip2recover bzip2recover.o

libbz2.a: $(OBJS)
	rm -f libbz2.a
	$(AR) cq libbz2.a $(OBJS)
	@if ( test -f $(RANLIB) -o -f /usr/bin/ranlib -o \
		-f /bin/ranlib -o -f /usr/ccs/bin/ranlib ) ; then \
		echo $(RANLIB) libbz2.a ; \
		$(RANLIB) libbz2.a ; \
	fi

## fixme: this wont work in cross-compile environments.
## we should find some more general way for running constraint test
## to allow an crosscompiling buildfarm to dock in (i.e. run the test
## on the target platform)
check: test
test: bzip2
	@cat words1
	./bzip2 -1  < sample1.ref > sample1.rb2
	./bzip2 -2  < sample2.ref > sample2.rb2
	./bzip2 -3  < sample3.ref > sample3.rb2
	./bzip2 -d  < sample1.bz2 > sample1.tst
	./bzip2 -d  < sample2.bz2 > sample2.tst
	./bzip2 -ds < sample3.bz2 > sample3.tst
	cmp sample1.bz2 sample1.rb2 
	cmp sample2.bz2 sample2.rb2
	cmp sample3.bz2 sample3.rb2
	cmp sample1.tst sample1.ref
	cmp sample2.tst sample2.ref
	cmp sample3.tst sample3.ref
	@cat words3

install-lib:
	mkdir -p $(DESTDIR)$(LIBDIR) $(DESTDIR)$(INCLUDEDIR)
	cp -f libbz2.a $(DESTDIR)$(LIBDIR)
	chmod a+r $(DESTDIR)$(LIBDIR)/libbz2.a
	cp -f bzlib.h $(DESTDIR)$(INCLUDEDIR)
	chmod a+r $(DESTDIR)$(INCLUDEDIR)/bzlib.h
	cp -f -P $(SHLIB) $(SONAME) $(LIBNAME) $(LIBNAME2) $(DESTDIR)$(LIBDIR)

MANPAGES=bzip2 bzgrep bzmore bzdiff bzexe
BINFILES=bzip2 bzgrep bzmore bzdiff bzexe bzip2recover

install-man:
	mkdir -p $(DESTDIR)$(MAN1DIR)
	for m in $(MANPAGES) ; do 	\
	    cp -f $$m.1 $(DESTDIR)$(MAN1DIR)	; \
	    chmod a+r $(DESTDIR)$(MAN1DIR)/$$m.1 ; \
	done
	echo ".so man1/bzgrep.1" > $(DESTDIR)$(MAN1DIR)/bzegrep.1
	echo ".so man1/bzgrep.1" > $(DESTDIR)$(MAN1DIR)/bzfgrep.1
	echo ".so man1/bzmore.1" > $(DESTDIR)$(MAN1DIR)/bzless.1
	echo ".so man1/bzdiff.1" > $(DESTDIR)$(MAN1DIR)/bzcmp.1
	echo ".so man1/bzip2.1"  > $(DESTDIR)$(MAN1DIR)/bunzip2.1
	echo ".so man1/bzip2.1"  > $(DESTDIR)$(MAN1DIR)/bzcat.1
	echo ".so man1/bzip2.1"  > $(DESTDIR)$(MAN1DIR)/bzip2recover.1

install-bin:
	mkdir -p $(DESTDIR)$(BINDIR)
	for f in $(BINFILES) ; do	\
	    cp -f $$f $(DESTDIR)$(BINDIR) ; \
	    chmod a+x $(DESTDIR)$(BINDIR)/$$f ;	\
	done
	$(STRIP) --strip-unneeded $(DESTDIR)$(BINDIR)/bzip2
	$(STRIP) --strip-unneeded $(DESTDIR)$(BINDIR)/bzip2recover
	ln -sf bzip2  $(DESTDIR)$(BINDIR)/bunzip2
	ln -sf bzip2  $(DESTDIR)$(BINDIR)/bzcat
	ln -sf bzgrep $(DESTDIR)$(BINDIR)/bzegrep
	ln -sf bzgrep $(DESTDIR)$(BINDIR)/bzfgrep
	ln -sf bzmore $(DESTDIR)$(BINDIR)/bzless
	ln -sf bzdiff $(DESTDIR)$(BINDIR)/bzcmp

install: bzip2 bzip2recover	install-bin install-lib install-man

clean: 
	rm -f *.o libbz2.a bzip2 bzip2recover bzip2-shared \
	sample1.rb2 sample2.rb2 sample3.rb2 \
	sample1.tst sample2.tst sample3.tst $(SONAME) $(LIBNAME) $(LIBNAME2)

blocksort.o: blocksort.c
	@cat words0
	$(CC) $(CFLAGS) -c blocksort.c
huffman.o: huffman.c
	$(CC) $(CFLAGS) -c huffman.c
crctable.o: crctable.c
	$(CC) $(CFLAGS) -c crctable.c
randtable.o: randtable.c
	$(CC) $(CFLAGS) -c randtable.c
compress.o: compress.c
	$(CC) $(CFLAGS) -c compress.c
decompress.o: decompress.c
	$(CC) $(CFLAGS) -c decompress.c
bzlib.o: bzlib.c
	$(CC) $(CFLAGS) -c bzlib.c
bzip2.o: bzip2.c
	$(CC) $(CFLAGS) -c bzip2.c
bzip2recover.o: bzip2recover.c
	$(CC) $(CFLAGS) -c bzip2recover.c

blocksort.pic.o: blocksort.c
	@cat words0
	$(CC) $(CFLAGS) $(CFLAGS_PIC) -o $@ -c $<
huffman.pic.o: huffman.c
	$(CC) $(CFLAGS) $(CFLAGS_PIC) -o $@ -c $<
crctable.pic.o: crctable.c
	$(CC) $(CFLAGS) $(CFLAGS_PIC) -o $@ -c $<
randtable.pic.o: randtable.c
	$(CC) $(CFLAGS) $(CFLAGS_PIC) -o $@ -c $<
compress.pic.o: compress.c
	$(CC) $(CFLAGS) $(CFLAGS_PIC) -o $@ -c $<
decompress.pic.o: decompress.c
	$(CC) $(CFLAGS) $(CFLAGS_PIC) -o $@ -c $<
bzlib.pic.o: bzlib.c
	$(CC) $(CFLAGS) $(CFLAGS_PIC) -o $@ -c $<
bzip2.pic.o: bzip2.c
	$(CC) $(CFLAGS) $(CFLAGS_PIC) -o $@ -c $<
bzip2recover.pic.o: bzip2recover.c
	$(CC) $(CFLAGS) $(CFLAGS_PIC) -o $@ -c $<

distclean: clean
	rm -f manual.ps manual.html manual.pdf

DISTNAME=bzip2-1.0.5
dist: check manual
	rm -f $(DISTNAME)
	ln -s -f . $(DISTNAME)
	tar cvf $(DISTNAME).tar \
	   $(DISTNAME)/blocksort.c \
	   $(DISTNAME)/huffman.c \
	   $(DISTNAME)/crctable.c \
	   $(DISTNAME)/randtable.c \
	   $(DISTNAME)/compress.c \
	   $(DISTNAME)/decompress.c \
	   $(DISTNAME)/bzlib.c \
	   $(DISTNAME)/bzip2.c \
	   $(DISTNAME)/bzip2recover.c \
	   $(DISTNAME)/bzlib.h \
	   $(DISTNAME)/bzlib_private.h \
	   $(DISTNAME)/Makefile \
	   $(DISTNAME)/LICENSE \
	   $(DISTNAME)/bzip2.1 \
	   $(DISTNAME)/bzip2.1.preformatted \
	   $(DISTNAME)/bzip2.txt \
	   $(DISTNAME)/words0 \
	   $(DISTNAME)/words1 \
	   $(DISTNAME)/words2 \
	   $(DISTNAME)/words3 \
	   $(DISTNAME)/sample1.ref \
	   $(DISTNAME)/sample2.ref \
	   $(DISTNAME)/sample3.ref \
	   $(DISTNAME)/sample1.bz2 \
	   $(DISTNAME)/sample2.bz2 \
	   $(DISTNAME)/sample3.bz2 \
	   $(DISTNAME)/dlltest.c \
	   $(DISTNAME)/manual.html \
	   $(DISTNAME)/manual.pdf \
	   $(DISTNAME)/manual.ps \
	   $(DISTNAME)/README \
	   $(DISTNAME)/README.COMPILATION.PROBLEMS \
	   $(DISTNAME)/README.XML.STUFF \
	   $(DISTNAME)/CHANGES \
	   $(DISTNAME)/libbz2.def \
	   $(DISTNAME)/libbz2.dsp \
	   $(DISTNAME)/dlltest.dsp \
	   $(DISTNAME)/makefile.msc \
	   $(DISTNAME)/unzcrash.c \
	   $(DISTNAME)/spewG.c \
	   $(DISTNAME)/mk251.c \
	   $(DISTNAME)/bzdiff \
	   $(DISTNAME)/bzdiff.1 \
	   $(DISTNAME)/bzexe \
	   $(DISTNAME)/bzexe.1 \
	   $(DISTNAME)/bzmore \
	   $(DISTNAME)/bzmore.1 \
	   $(DISTNAME)/bzgrep \
	   $(DISTNAME)/bzgrep.1 \
	   $(DISTNAME)/Makefile-libbz2_so \
	   $(DISTNAME)/bz-common.xsl \
	   $(DISTNAME)/bz-fo.xsl \
	   $(DISTNAME)/bz-html.xsl \
	   $(DISTNAME)/bzip.css \
	   $(DISTNAME)/entities.xml \
	   $(DISTNAME)/manual.xml \
	   $(DISTNAME)/format.pl \
	   $(DISTNAME)/xmlproc.sh
	gzip -v $(DISTNAME).tar

# For rebuilding the manual from sources on my SuSE 9.1 box

MANUAL_SRCS= 	bz-common.xsl bz-fo.xsl bz-html.xsl bzip.css \
		entities.xml manual.xml 

manual: manual.html manual.ps manual.pdf

manual.ps: $(MANUAL_SRCS)
	./xmlproc.sh -ps manual.xml

manual.pdf: $(MANUAL_SRCS)
	./xmlproc.sh -pdf manual.xml

manual.html: $(MANUAL_SRCS)
	./xmlproc.sh -html manual.xml

bzip2-shared: $(LIBNAME)
	$(CC) $(LDFLAGS) $(CFLAGS) -o bzip2-shared bzip2.c $(LIBNAME)
